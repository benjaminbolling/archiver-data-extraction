# Code for getting the data and then writing it to a CSV file
from epicsarchiver import ArchiverAppliance # Can be installed as follows: >> pip install py-epicsarchiver -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
import csv

def getData(pvs,starttime,endtime,strftimeformat,archiveAddr,archiveN):
    archiver = ArchiverAppliance(archiveAddr)

    for pv in pvs:
        status = archiver.get_pv_status(pv)
        print('{} : {}'.format(status[0]['pvName'],status[0]['status']))
        if status[0]['status'] == 'Being archived':
            df = archiver.get_data(pv, start=starttime, end=endtime) # pandas dataframe

            values_raw = df.values.tolist()
            timestamps = df.index.strftime(strftimeformat).tolist()

            # each value is a list array object ; convert them to floats
            values = [value[0] for value in values_raw]

            fn = 'archiver{}_data__{}__{}__{}'.format(str(archiveN),str(pv),str(starttime),str(endtime))

            with open('data/'+fn+'.csv', 'w') as f:
                writer = csv.writer(f)
                writer.writerow(['timestamp',str(pv)])
                writer.writerows(zip(timestamps, values))
