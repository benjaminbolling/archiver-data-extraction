# Get Data from Archiver
A simple Python-based tool for extracting data from the ESS archiver into *.csv*-files.

## Setup
0. Clone this package to a local folder on the computer.
1. In order to use this tool, ensure that Python 3.x (3.7 is recommended) is installed on the computer.
2. Check Python version used with the PIP package manager such that it points to the correct Python version (pip -V).
3. Use PIP to install the package required py-epicsarchiver package:

    pip install py-epicsarchiver -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple

## User guide
0. Prepare a list of PVs in a *.txt*-file.
1. Open the file *run.py* in a text editor.
2. Define start- and end-time of the data collection in the provided format.
3. Write the name of the file with the list of PVs or edit the pvs.txt file.
4. Ensure the string time format is as desired
5. Ensure the archiver address is correct
6. Exit the text-editor and execute *python run.py*
7. If any of the PVs are being archived, the data will be extracted with timestamps and inserted into a *.csv*-file in the *data/* folder with naming defined as: 'archiver[number]_data__[pv-name]__[starttime]__[endtime]'
