from datetime import datetime
import getData

starttime = '2020-07-04 13:00'
endtime = datetime.utcnow()

# List of PVs (Example file has one archived and one not-archived PV)
pvs = open('pvs.txt', "r").read().split('\n')

# String time format for output file and address of the archive appliance
strftimeformat = "%Y-%m-%d_%H:%M:%S"

for n in range(1,3):
    archiveAddr = 'archiver-0{}.tn.esss.lu.se'.format(n)
    getData.getData(pvs,starttime,endtime,strftimeformat,archiveAddr,n)
